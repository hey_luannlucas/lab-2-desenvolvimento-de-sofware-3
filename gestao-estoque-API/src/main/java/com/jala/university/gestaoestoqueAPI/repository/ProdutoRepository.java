package com.jala.university.gestaoestoqueAPI.repository;

import com.jala.university.gestaoestoqueAPI.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    // Métodos de consulta personalizados, se necessário
}
