package com.jala.university.gestaoestoqueAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoEstoqueApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoEstoqueApiApplication.class, args);
	}

}
