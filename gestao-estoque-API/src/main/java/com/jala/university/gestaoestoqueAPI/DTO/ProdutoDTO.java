package com.jala.university.gestaoestoqueAPI.DTO;


import com.jala.university.gestaoestoqueAPI.entity.Produto;
import jakarta.validation.constraints.*;

import java.math.BigDecimal;

public class ProdutoDTO {
    @NotBlank(message = "O nome do produto não pode estar em branco")
    private String nome;

    @NotBlank(message = "A descrição do produto não pode estar em branco")
    private String descricao;

    @NotNull(message = "O preço do produto não pode estar em branco")
    @Positive(message = "O preço do produto deve ser positivo")
    private BigDecimal preco;

    @NotNull(message = "A quantidade em estoque do produto não pode estar em branco")
    @PositiveOrZero(message = "A quantidade em estoque do produto deve ser positiva ou zero")
    private int quantidadeEstoque;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public int getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(int quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public static ProdutoDTO fromEntity(Produto produto) {
        ProdutoDTO dto = new ProdutoDTO();
        dto.setNome(produto.getNome());
        dto.setDescricao(produto.getDescricao());
        dto.setPreco(produto.getPreco());
        dto.setQuantidadeEstoque(produto.getQuantidadeEstoque());
        return dto;
    }
}
