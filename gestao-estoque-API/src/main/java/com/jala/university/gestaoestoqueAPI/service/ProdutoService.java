package com.jala.university.gestaoestoqueAPI.service;


import com.jala.university.gestaoestoqueAPI.DTO.ProdutoDTO;
import com.jala.university.gestaoestoqueAPI.entity.Produto;
import com.jala.university.gestaoestoqueAPI.exeptions.ProdutoNotFoundException;
import com.jala.university.gestaoestoqueAPI.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public ProdutoDTO adicionarProduto(ProdutoDTO produtoDTO) {
        Produto produto = new Produto();
        produto.setNome(produtoDTO.getNome());
        produto.setDescricao(produtoDTO.getDescricao());
        produto.setPreco(produtoDTO.getPreco());
        produto.setQuantidadeEstoque(produtoDTO.getQuantidadeEstoque());
        Produto savedProduto = produtoRepository.save(produto);
        return ProdutoDTO.fromEntity(savedProduto);
    }

    public ProdutoDTO atualizarProduto(Long id, ProdutoDTO produtoDTO) {
        if (!produtoRepository.existsById(id)) {
            throw new ProdutoNotFoundException("Produto não encontrado");
        }

        Produto produto = produtoRepository.findById(id).orElse(null);
        produto.setNome(produtoDTO.getNome());
        produto.setDescricao(produtoDTO.getDescricao());
        produto.setPreco(produtoDTO.getPreco());
        produto.setQuantidadeEstoque(produtoDTO.getQuantidadeEstoque());
        Produto updatedProduto = produtoRepository.save(produto);
        return ProdutoDTO.fromEntity(updatedProduto);
    }

    public void excluirProduto(Long id) {
        // Verifique se o produto existe
        if (!produtoRepository.existsById(id)) {
            throw new ProdutoNotFoundException("Produto não encontrado");
        }
        produtoRepository.deleteById(id);
    }

    public ProdutoDTO consultarProduto(Long id) {
        Produto produto = produtoRepository.findById(id).orElse(null);
        if (produto == null) {
            throw new ProdutoNotFoundException("Produto não encontrado");
        }
        return ProdutoDTO.fromEntity(produto);
    }

    public List<ProdutoDTO> listarProdutos() {
        List<Produto> produtos = produtoRepository.findAll();
        return produtos.stream().map(ProdutoDTO::fromEntity).collect(Collectors.toList());
    }

}
