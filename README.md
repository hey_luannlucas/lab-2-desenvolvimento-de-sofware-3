# Gestão de Estoque API

Este é um projeto de exemplo que demonstra uma API para gestão de estoque de produtos. A API permite adicionar, atualizar, excluir, consultar e listar produtos no estoque.

## Tecnologias Utilizadas

- **Spring Boot:** Framework utilizado para criar a aplicação em Java de forma rápida e fácil.
- **Spring Data JPA:** Biblioteca que facilita o acesso a dados usando a tecnologia JPA.
- **Hibernate:** Framework ORM utilizado para mapear objetos Java para o banco de dados relacional.
- **Swagger:** Utilizado para documentar a API de forma interativa e fácil de entender.
- **Jakarta Validation:** Usado para validar os dados de entrada na API.
- **H2 Database:** Banco de dados em memória utilizado para fins de demonstração.

## Estrutura do Projeto

O projeto é dividido em pacotes de acordo com a sua responsabilidade:

- **controller:** Contém os controladores da API que definem os endpoints.
- **service:** Contém a lógica de negócio da aplicação.
- **DTO:** Contém os objetos de transferência de dados usados para serialização e desserialização de dados entre a API e o cliente.
- **entity:** Contém as entidades JPA que representam as tabelas do banco de dados.
- **repository:** Contém as interfaces de repositório JPA para acessar os dados do banco de dados.

## Funcionalidades

- **Adicionar Produto:** Permite adicionar um novo produto ao estoque.
- **Atualizar Produto:** Permite atualizar as informações de um produto existente no estoque.
- **Excluir Produto:** Permite excluir um produto do estoque.
- **Consultar Produto por ID:** Permite consultar um produto específico com base no seu ID.
- **Listar Produtos:** Permite listar todos os produtos disponíveis no estoque.

## Validação de Entradas

As entradas na API são validadas com base nas regras definidas nos objetos de transferência de dados (DTO). Caso ocorra alguma violação nas regras de validação, a API retorna uma resposta HTTP 400 (Bad Request) com mensagens de erro detalhadas.

## Documentação da API

A documentação da API é gerada automaticamente usando o Swagger. Acesse a documentação em [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html) após iniciar a aplicação.
<br>
![documentacao swagger](assets/img.png)

## Executando o Projeto

Para executar o projeto localmente, você pode clonar este repositório e executar o seguinte comando na raiz do projeto:

```bash
mvn spring-boot:run
```

Certifique-se de ter o Maven instalado em seu sistema. O projeto será executado em [http://localhost:8080](http://localhost:8080).

## Contato

Se você tiver alguma dúvida ou sugestão, entre em contato:

- **Email:** Luann.deSousa@jala.university
- **Nome:** Lab 2

